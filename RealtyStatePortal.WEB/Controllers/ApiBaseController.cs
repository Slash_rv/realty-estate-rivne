﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RealtyStatePortal.Data.Contracts;

namespace RealtyStatePortal.WEB.Controllers
{
    public class ApiBaseController : ApiController
    {
        protected IRealtyStatePortalUOW Uow { get; set; }
    }
}
