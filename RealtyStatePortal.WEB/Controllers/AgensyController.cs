﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RealtyStatePortal.Data.Contracts;
using RealtyStatePortal.Model;

namespace RealtyStatePortal.WEB.Controllers
{
    public class AgensyController : ApiBaseController
    {
        public AgensyController(IRealtyStatePortalUOW uow)
       {
           Uow = uow;
       }
        // GET api/Agensy
        public IEnumerable<Agensy> Get()
        {
            return Uow.Agencies.GetAll().OrderBy(s => s.AgensyName);
        }
        // GET api/Agencies/5
        public string Get(int id)
        {
            return "value";
        }
        // POST api/Agencies
        public void Post([FromBody]string value)
        {
        }
        // PUT api/Agencies/5
        public void Put(int id, [FromBody]string value)
        {
        }
        // DELETE api/Agencies/5
        public void Delete(int id)
        {
        }
    }
}
