﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealtyStatePortal.Model
{
    public class Agensy
    {
        public int ID { get; set; }
        public string AgensyName { get; set; }
        public string AgensyAdress { get; set; }
        public string AgensyPhone { get; set; }
        public string AgensyMail { get; set; }
        public string AgensyPas { get; set; }

        public ICollection<AgensyRieltor> Rieltors {get;set;}

        public ICollection<AgensyReview> Reviews { get; set; }
    
    }
}
