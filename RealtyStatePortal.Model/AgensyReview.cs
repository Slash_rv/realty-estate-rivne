﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealtyStatePortal.Model
{
    public class AgensyReview
    {
        public int ID { get; set; }
        public string PersonName { get; set; }
        public string PersonMail { get; set; }
        public string PersonalPhone { get; set; }
        public string PersonalReview { get; set; }
        public DateTime ReviewDate { get; set; }
    }
}
