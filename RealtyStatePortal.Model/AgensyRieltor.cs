﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealtyStatePortal.Model
{
    public class AgensyRieltor
    {
        public int ID { get; set; }
        public string RieltorName { get; set; }
        public string RieltorPhone { get; set; }
        public string RieltorPas { get; set; }
    }
}
