﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RealtyStatePortal.Model;

namespace RealtyStatePortal.Data.Contracts
{
   public interface IRealtyStatePortalUOW
    {
        void Commit();
        IRepository<Agensy> Agencies { get; }
        IRepository<AgensyReview> AgensyReviews { get; }
        IRepository<AgensyRieltor> AgensyRieltors { get; }
    }
}
