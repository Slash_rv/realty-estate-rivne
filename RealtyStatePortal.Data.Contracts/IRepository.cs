﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealtyStatePortal.Data.Contracts
{
    public interface IRepository<T> where T : class
    {
        
            //To query using LINQ
            IQueryable<T> GetAll();
            //Returning Agensy or Review by id
            T GetById(int id);
            //Adding Agensy or Review
            void Add(T entity);
            //Updating Agensy or Review
            void Update(T entity);
            //Deleting Agensy or Review
            void Delete(T entity);
            //Deleting Agensy or Review by id
            void Delete(int id);
    }
}

