﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RealtyStatePortal.Model;
namespace RealtyStatePortal.Data.SampleData
{
    public class RealtyStatePortalInitializer : DropCreateDatabaseIfModelChanges<RealtyStatePortalDbContext>
    {
        protected override void Seed(RealtyStatePortalDbContext context)
        {
            context.Agencies.AddOrUpdate(r => r.AgensyName,
                new Agensy
                {
                    AgensyName = "Blagodim",
                    AgensyAdress = "Rivne",
                    AgensyPhone = "380976459819",
                    AgensyMail = "blagodim@gmail.com",
                    AgensyPas = "123",
                    Rieltors = new List<AgensyRieltor>{
                new AgensyRieltor{
                    RieltorName="Anna",RieltorPhone="380976459819",RieltorPas="123"},
                new AgensyRieltor{
                    RieltorName="Lyuda",RieltorPhone="380976459820",RieltorPas="123"}
                },
                    Reviews = new List<AgensyReview>{
                new AgensyReview{
                    PersonName="Vasya",PersonMail="vasya123@gmail.com",PersonalPhone="3890981349821",PersonalReview="Good Work"},
                new AgensyReview{
                    PersonName="Sveta",PersonMail="sveta123@gmail.com",PersonalPhone="3890981349822",PersonalReview="Norm Work"}
                }
                });

        }
    }
}
