﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RealtyStatePortal.Model;
using RealtyStatePortal.Data.SampleData;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Threading.Tasks;
namespace RealtyStatePortal.Data
{
    public class RealtyStatePortalDbContext:DbContext
    {
        public RealtyStatePortalDbContext() : base(nameOrConnectionString: "RealtyStatePortalDb") { }
        public DbSet<Agensy> Agencies { get; set; }
        public DbSet<AgensyReview> AgensyReviews { get; set; }
        public DbSet<AgensyRieltor> AgensyRieltors { get; set; }

        static RealtyStatePortalDbContext()
        {
            Database.SetInitializer(new RealtyStatePortalInitializer());
        }
        //setting EF Convetions
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //use singular table names 
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            base.OnModelCreating(modelBuilder);
        }
    }
}
