﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RealtyStatePortal.Model;
using RealtyStatePortal.Data.Helpers;
using RealtyStatePortal.Data.Contracts;

namespace RealtyStatePortal.Data
{
    public class RealtyStatePortalUow : IRealtyStatePortalUOW,IDisposable
    {
        public RealtyStatePortalUow(IRepositoryProvider repositoryProvider)
        {
            CreateDbContext();
            repositoryProvider.DbContext = DbContext;
            RepositoryProvider = repositoryProvider;
        }
        public IRepository<Agensy> Agencies { get { return GetStandardRepo<Agensy>(); } }
        public IRepository<AgensyReview> AgensyReviews { get { return GetStandardRepo<AgensyReview>(); } }
        public IRepository<AgensyRieltor> AgensyRieltors { get { return GetStandardRepo<AgensyRieltor>(); } }
        public void Commit()
        {
            DbContext.SaveChanges();
        }
        protected void CreateDbContext()
        {
            DbContext = new RealtyStatePortalDbContext();
            //Do Not enable proxy entities
            DbContext.Configuration.ProxyCreationEnabled = false;
            //Load navigation property explicitly
            DbContext.Configuration.LazyLoadingEnabled = false;
            DbContext.Configuration.ValidateOnSaveEnabled = false;
        }
        protected IRepositoryProvider RepositoryProvider { get; set; }
        private IRepository<T> GetStandardRepo<T>() where T : class
        {
            return RepositoryProvider.GetRepositoryForEntityType<T>();
        }
        private T GetRepo<T>() where T : class
        {
            return RepositoryProvider.GetRepository<T>();
        }
        private RealtyStatePortalDbContext DbContext { get; set; }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (DbContext != null)
                {
                    DbContext.Dispose();
                }
            }
        }
    }
}
